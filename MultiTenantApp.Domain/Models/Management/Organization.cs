﻿using MultiTenantApp.Domain.Models.General;

namespace MultiTenantApp.Domain.Models.Management
{
    public class Organization : BaseProperties
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string SlugTenant { get; set; } = string.Empty;
    }
}
