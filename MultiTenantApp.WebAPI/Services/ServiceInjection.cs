﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using MultiTenantApp.Application.Commands.ProductsDatabase.Product.Writer;
using MultiTenantApp.Application.Queries.ProductsDatabase.Product;
using MultiTenantApp.Application.Repositories;
using MultiTenantApp.Application.Repositories.Configuration;
using MultiTenantApp.Application.Services;
using MultiTenantApp.Domain.Models.Management;
using MultiTenantApp.Domain.Models.Products.Reader;
using MultiTenantApp.Domain.Models.Products.Writer;
using System.Text;

namespace MultiTenantApp.WebAPI.Services
{
    public static class ServiceInjection
    {
        public static void AddCustomInjections(this IServiceCollection services)
        {

            AddCustomServices(services);
            AddCustomRepositories(services);
            AddCommands(services);
            AddQueries(services);
        }

        public static IServiceCollection AddCustomServices(IServiceCollection services)
        {
            services.AddScoped<IDatabaseCreationService, DatabaseCreationService>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = "yourissuer",
                            ValidAudience = "youraudience",
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("your_secret_key"))
                        };
                    });
            //services.AddScoped<IMyCustomService, MyCustomService>();

            return services;
        }

        public static IServiceCollection AddCustomRepositories(IServiceCollection services)
        {
            services.AddScoped<IRepositoryGeneric<ProductR>, RepositoryGenericProductReader<ProductR>>();
            services.AddScoped<IRepositoryGeneric<ProductW>, RepositoryGenericProductWriter<ProductW>>();
            services.AddScoped<IRepositoryGeneric<Organization>, RepositoryGenericManagement<Organization>>();
            services.AddScoped<IRepositoryGeneric<User>, RepositoryGenericManagement<User>>();

            return services;
        }

        public static IServiceCollection AddCommands(IServiceCollection services)
        {
            services.AddScoped<CreateProductCommandHandler>();
            return services;
        }

        public static IServiceCollection AddQueries(IServiceCollection services)
        {
            services.AddScoped<GetAllProductsQueryHandler>();
            return services;
        }
    }
}
