﻿using MediatR;
using MultiTenantApp.Application.Shared.Management.Organization;

namespace MultiTenantApp.Application.Commands.ManagementDatabase.User
{
    public class DeleteOrganizationCommand : IRequest<OrganizationDto>
    {
        public int OrganizationId { get; set; }
    }
}