﻿using MediatR;
using MultiTenantApp.Application.Shared.Management.Organization;

namespace MultiTenantApp.Application.Commands.ManagementDatabase.User
{
    public class CreateOrganizationCommand : IRequest<OrganizationDto>
    {
        public string Name { get; set; } = string.Empty;
        public string SlugTenant { get; set; } = string.Empty;
    }
}
