﻿using MediatR;
using MultiTenantApp.Application.Shared.Management.User;

namespace MultiTenantApp.Application.Commands.ManagementDatabase.User
{
    public class DeleteUserCommand : IRequest<UserDto>
    {
        public int UserId { get; set; }
    }
}