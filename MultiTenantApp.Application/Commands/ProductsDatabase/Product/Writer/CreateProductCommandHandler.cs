﻿using MediatR;
using MultiTenantApp.Application.Commands.ProductsDatabase.Product.Reader;
using MultiTenantApp.Application.Repositories.Configuration;
using MultiTenantApp.Domain.Models.Products.Writer;

namespace MultiTenantApp.Application.Commands.ProductsDatabase.Product.Writer
{
    public class CreateProductCommandHandler
    {
        private readonly IRepositoryGeneric<ProductW> repository;
        private readonly IMediator mediator;

        public CreateProductCommandHandler(
            IRepositoryGeneric<ProductW> repository,
            IMediator mediator)
        {
            this.repository = repository;
            this.mediator = mediator;
        }

        public async Task Handle(CreateProductCommand command)
        {
            var product = new ProductW
            {
                Name = command.Name,
                Description = command.Description,
                Duration = command.Duration,
                CreatedDateTimeOffset = DateTimeOffset.UtcNow,
            };

            repository.Create(product);
            await repository.SaveChangesAsync();

            var replicateCommand = new ReplicateProductCommand(
                product.Id,
                product.Name,
                product.Description,
                product.Duration);

            await mediator.Send(replicateCommand);
        }
    }
}
