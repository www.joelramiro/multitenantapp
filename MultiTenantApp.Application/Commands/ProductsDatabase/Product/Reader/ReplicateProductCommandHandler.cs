﻿using MediatR;
using MultiTenantApp.Application.Repositories.Configuration;
using MultiTenantApp.Domain.Models.Products.Reader;

namespace MultiTenantApp.Application.Commands.ProductsDatabase.Product.Reader
{
    public class ReplicateProductCommandHandler : IRequestHandler<ReplicateProductCommand, Unit>
    {
        private readonly IRepositoryGeneric<ProductR> repository;

        public ReplicateProductCommandHandler(IRepositoryGeneric<ProductR> repository)
        {
            this.repository = repository;
        }

        public async Task<Unit> Handle(ReplicateProductCommand command, CancellationToken cancellationToken)
        {
            var product = new ProductR
            {
                Name = command.Name,
                Description = command.Description,
                Duration = command.Duration,
            };

            repository.Create(product);
            await repository.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
