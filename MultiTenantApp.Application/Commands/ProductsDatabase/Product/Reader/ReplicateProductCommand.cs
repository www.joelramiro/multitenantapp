﻿using MediatR;

namespace MultiTenantApp.Application.Commands.ProductsDatabase.Product.Reader
{
    public class ReplicateProductCommand : IRequest<Unit>
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TimeSpan Duration { get; set; }

        public ReplicateProductCommand(int productId, string name, string description, TimeSpan duration)
        {
            ProductId = productId;
            Name = name;
            Description = description;
            Duration = duration;
        }
    }
}
