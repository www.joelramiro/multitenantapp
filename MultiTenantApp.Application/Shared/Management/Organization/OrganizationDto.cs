﻿namespace MultiTenantApp.Application.Shared.Management.Organization
{
    public class OrganizationDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
