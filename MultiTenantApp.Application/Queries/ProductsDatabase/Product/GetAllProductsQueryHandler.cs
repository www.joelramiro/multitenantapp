﻿using Microsoft.EntityFrameworkCore;
using MultiTenantApp.Application.Repositories.Configuration;
using MultiTenantApp.Domain.Models.Products.Reader;

namespace MultiTenantApp.Application.Queries.ProductsDatabase.Product
{
    public class GetAllProductsQueryHandler
    {
        private readonly IRepositoryGeneric<ProductR> repository;

        public GetAllProductsQueryHandler(IRepositoryGeneric<ProductR> repository)
        {
            this.repository = repository;
        }

        public async Task<List<ProductR>> Handle(GetAllProductsQuery query)
        {
            return await repository.All().ToListAsync();
        }
    }
}
