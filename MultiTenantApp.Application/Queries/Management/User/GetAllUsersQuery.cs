﻿using MediatR;
using MultiTenantApp.Application.Shared.Management.User;

namespace MultiTenantApp.Application.Queries.Management.User
{
    public class GetAllUsersQuery : IRequest<List<UserDto>>
    {
    }
}
