﻿using MediatR;
using MultiTenantApp.Application.Exceptions;
using MultiTenantApp.Application.Repositories.Configuration;
using Microsoft.EntityFrameworkCore;
using MultiTenantApp.Application.Shared.Management.User;

namespace MultiTenantApp.Application.Queries.Management.User
{
    public class GetUserByIdQuery : IRequest<UserDto>
    {
        public int UserId { get; set; }
    }
    public class GetUserByIdQueryHandler : IRequestHandler<GetUserByIdQuery, UserDto>
    {
        private readonly IRepositoryGeneric<Domain.Models.Management.User> _repository;

        public GetUserByIdQueryHandler(IRepositoryGeneric<Domain.Models.Management.User> repository)
        {
            _repository = repository;
        }

        public async Task<UserDto> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
        {
            var User = await _repository.All()
                .Include(user => user.Organization)
                .FirstOrDefaultAsync(item => item.Id == request.UserId);

            if (User == null)
            {
                throw new NotFoundException($"User with id {request.UserId} not found.");
            }

            var UserDto = new UserDto
            {
                Id = User.Id,
                Email = User.Email,
                Organization = User.Organization!.Name,
                OrganizationId = User.OrganizationId,
            };

            return UserDto;
        }
    }
}