﻿using MediatR;
using MultiTenantApp.Application.Shared.Management.Organization;

namespace MultiTenantApp.Application.Queries.Management.Organization
{
    public class GetAllOrganizationsQuery : IRequest<List<OrganizationDto>>
    {
    }
}
