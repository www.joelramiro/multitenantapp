﻿using MediatR;
using MultiTenantApp.Application.Shared.Management.Organization;

namespace MultiTenantApp.Application.Queries.Management.Organization
{
    public class GetOrganizationByIdQuery : IRequest<OrganizationDto>
    {
        public int OrganizationId { get; set; }
    }
}