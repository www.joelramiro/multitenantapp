﻿using MultiTenantApp.Application.Repositories.Configuration;
using MultiTenantApp.Infrastructure.Context.Product;

namespace MultiTenantApp.Application.Repositories
{
    public class RepositoryGenericProductWriter<TEntity> : RepositoryGeneric<TEntity, ProductContextWriter> where TEntity : class
    {
        public RepositoryGenericProductWriter(ProductContextWriter context) : base(context)
        {
        }
    }
}