﻿using MultiTenantApp.Application.Repositories.Configuration;
using MultiTenantApp.Infrastructure.Context.Product;

namespace MultiTenantApp.Application.Repositories
{
    public class RepositoryGenericProductReader<TEntity> : RepositoryGeneric<TEntity, ProductContextReader> where TEntity : class
    {
        public RepositoryGenericProductReader(ProductContextReader context) : base(context)
        {
        }
    }
}