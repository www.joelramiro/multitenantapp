﻿using MultiTenantApp.Application.Repositories.Configuration;
using MultiTenantApp.Infrastructure.Context.Management;

namespace MultiTenantApp.Application.Repositories
{
    public class RepositoryGenericManagement<TEntity> : RepositoryGeneric<TEntity, ManagementContext> where TEntity : class
    {
        public RepositoryGenericManagement(ManagementContext context) : base(context)
        {
        }
    }
}